import React from "react";
import NavbarComponent from "../../components/Navbar";
import TableComponent from "../../components/Table";
import InputForm from "../../components/InputForm";

function Student() {
  return (
    <div>
      <NavbarComponent />
      <InputForm />
      <TableComponent />
    </div>
  );
}

export default Student;
