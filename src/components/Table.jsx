import React from "react";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

function TableComponent() {
  return (
    <div style={{ padding: "2%" }}>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Year Level</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>mark.otto@example.com</td>
            <td>1st Year</td>
            <td>
              <Button variant="info">Edit</Button>{" "}
              <Button variant="danger">Delete</Button>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>mark.otto@example.com</td>
            <td>1st Year</td>
            <td>
              <Button variant="info">Edit</Button>{" "}
              <Button variant="danger">Delete</Button>
            </td>
          </tr>
          <tr>
            <td>3</td>
            <td>Finn</td>
            <td>Sanchez</td>
            <td>finn@example.com</td>
            <td>3rd Year</td>
            <td>
              <Button variant="info">Edit</Button>{" "}
              <Button variant="danger">Delete</Button>
            </td>
          </tr>
          <tr>
            <td>4</td>
            <td>Rick</td>
            <td>Sanchez</td>
            <td>rick@example.com</td>
            <td>4th Year</td>
            <td>
              <Button variant="info">Edit</Button>{" "}
              <Button variant="danger">Delete</Button>
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

export default TableComponent;
