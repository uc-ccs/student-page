import React from 'react';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";


function InputForm() {
  return (
    <div className="mb-3" style={{ paddingLeft: "25%", paddingRight: "25%", }}>
    <Form>
      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <Form.Label>First name</Form.Label>
        <Form.Control type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <Form.Label>Last name</Form.Label>
        <Form.Control type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="" />
      </Form.Group>
      <Form.Label>Year Level</Form.Label>
      <Form.Select aria-label="Year Level">
        <option value="1">1st Year</option>
        <option value="2">2nd Year</option>
        <option value="3">3rd year</option>
        <option value="3">4th year</option>
      </Form.Select>
      <Form.Group className="mt-3" controlId="exampleForm.ControlInput1">
        <Button variant="primary">Save</Button>
      </Form.Group>
    </Form>
  </div>
  );
}


export default InputForm;