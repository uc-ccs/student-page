import {
    createBrowserRouter,
  } from "react-router-dom";

  import Student from "../pages/Student";

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Student />,
    },
  ]);

  export default router